import enum
from datetime import datetime


class Type(enum.Enum):
    PRIVATE = 'private'
    MEETING = 'meeting'
    CORPORATE = 'corporate'
    OTHER = 'other'


class Event:

    def __init__(self, date: datetime = None, type: Type = Type.CORPORATE, members: [] = None, place: str = None,
                 name: str = None):
        self._date = date
        self._type = type
        self._members = members
        self._place = place
        self._name = name

    @property
    def date(self):
        return self._date

    @property
    def type(self):
        return self._type

    @property
    def members(self):
        return self._members

    @property
    def place(self):
        return self._place

    @property
    def name(self):
        return self._name

    @staticmethod
    def print(self):
        print(self.date, self.type, self.name, self.members, self.place)

    def toDict(self):
        return {
            'date': self._date.isoformat(),
            'type': self._type,
            'members': self._members,
            'place': self._place,
            'name': self._name
        }

    @staticmethod
    def from_dict(event_dict):
        event = Event(
            type=Type(event_dict['type']),
            date=datetime.fromisoformat(event_dict['date']),
            members=event_dict['members'],
            place=event_dict['place'],
            name=event_dict['name']
        )
        return event
