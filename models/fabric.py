import enum
from datetime import timedelta
import random as rnd
from faker import Faker

from models.event import Event


class Type(enum.Enum):
    PRIVATE = 'private'
    MEETING = 'meeting'
    CORPORATE = 'corporate'
    OTHER = 'other'


fake = Faker()
listMembers = []
for i in range(10):
    listMembers.append(fake.name())


def random_date(start, end):
    delta = end - start
    int_delta = (timedelta.total_seconds(delta))
    random_second = rnd.randrange(int(int_delta))
    return start + timedelta(seconds=random_second)


class Fabric:
    def __init__(self, leftTime, rightTime):
        self._left_time = leftTime
        self._right_time = rightTime

    def create_events(self, num):
        for i in range(num):
            yield self.create_event()

    def create_event(self):
        rnd.shuffle(listMembers)
        l = []
        for i in range(3): l.append(listMembers[i])
        return Event(
            date=random_date(self._left_time, self._right_time),
            type=rnd.choice(list(Type)).name,
            members=l,
            place=fake.sha256()[0:20],
            name=fake.sha256()[0:20]
        )
