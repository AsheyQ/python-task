# Python training task

Task:
You need to write a program that groups a list of events by date and saves it to a json file. To generate events we need to write a factory that accepts a date range for which we need to randomly generate events, the time and time zone are also random.
The generated event has the following attributes:
- date and time with timezone
- event type (private, meeting, corporate, other)
- event name (random sequence of letters and numbers separated by spaces, length of one sequence not more than 20 characters, length of the field not more  than 100 characters)
- event participants, a list of names of people invited to the event
- location (can be zoom, telegram, or address, which we generate as a random sequence of characters).

The result should be a json file where events are grouped and sorted by date, each date has a list of events for that date.
Events with type other should not be included in the final file.

Requirements:
- event type is described via enum
- working with dates and time must be in utc using datetime package, date format - ISO.
- unit-tests must be written using pytest, in which the logic of generating and forming summary data must be covered.
- output file name, start and end of date range for events can be passed through command line parameters, use argparse package
- do not use procedural programming - all through classes
- python code style should be followed
