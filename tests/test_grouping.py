from datetime import datetime, timedelta
from itertools import groupby

import pytest
from hamcrest import assert_that, contains_inanyorder

from models.event import Event
from models.fabric import Fabric
from utils.grouping import EventGrouper

fabric = Fabric(datetime.today() - timedelta(days=30), datetime.today())
grouper = EventGrouper
events = list(fabric.create_events(100))


@pytest.fixture
def events_grouped_by_date():
    events_ = []
    for event in events:
        if event.toDict()['type'] == 'OTHER':
            continue
        events_.append(event)

    events_by_date = {k: list(v) for k, v in
                      groupby(sorted(events_, key=lambda p: p.date), lambda p: p.date.strftime("%Y-%m-%d"))}

    events_by_date_ = {data: [Event.toDict(p) for p in gp] for data, gp in events_by_date.items()}

    yield events_by_date_
    print('teardown "events_grouped_by_date"')


def test_grouping_by_sex(events_grouped_by_date):
    grouped_by_date_result = EventGrouper.groupByDate(events)

    assert not set(grouped_by_date_result.keys()).symmetric_difference(events_grouped_by_date.keys()), "группы разные"
    for gr, pers in events_grouped_by_date.items():
        assert_that(pers, contains_inanyorder(*grouped_by_date_result[gr]))
