
class EventGrouper:

    @staticmethod
    def groupByDate(self):
        data = {}
        events = []
        for event in self:
            if event.toDict()['type'] == 'OTHER':
                continue
            events.append(event.toDict())
            data[event.toDict()['date'][:-16]] = []
        data = dict(sorted(data.items()))

        for event in events:
            body = {
                'date': event['date'],
                'type': event['type'],
                'members': event['members'],
                'place': event['place'],
                'name': event['name']
            }
            data[event['date'][:-16]].extend([body])
        return data
