import argparse
from datetime import datetime, timedelta

from models.fabric import Fabric
from utils.grouping import EventGrouper
from utils.dumping import EventDumper


class Main:
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", "--leftDate", default=datetime.today() - timedelta(days=30))
    parser.add_argument("-r", "--rightDate", default=datetime.today())
    parser.add_argument("-filename", default='data.json.txt')

    args = parser.parse_args()

    fabric = Fabric(args.leftDate, args.rightDate)
    events = list(fabric.create_events(100))

    print(EventDumper.dumps([p.toDict() for p in events]))
    grouper = EventGrouper
    data = grouper.groupByDate(events)
